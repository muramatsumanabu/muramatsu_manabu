package service;

import java.sql.Connection;
import java.util.List;

import bean.Branch;
import dao.BranchDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class BranchService {

	public List<Branch> select() {

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();
			return new BranchDao().select(connection);

		} catch (Exception e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}
}
