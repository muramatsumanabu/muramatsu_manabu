package service;

import java.sql.Connection;
import java.util.List;

import bean.User;
import bean.UserBranchDepartment;
import dao.UserBranchDepartmentDao;
import dao.UserDao;
import utils.CipherUtil;
import utils.CloseableUtil;
import utils.DBUtil;

public class UserService {

	public void insert(User user) {

		Connection connection = null;
		try {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			connection = DBUtil.getConnection();
			new UserDao().insert(connection, user);
			DBUtil.commit(connection);

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}

	public User select(String account, String password) {

		Connection connection = null;
		try {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(password);

			connection = DBUtil.getConnection();
			User user = new UserDao().select(connection, account, encPassword);
			DBUtil.commit(connection);

			return user;

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}

	public User select(String account) {

		Connection connection = null;
		try {
			connection = DBUtil.getConnection();
			User user = new UserDao().select(connection, account);
			DBUtil.commit(connection);

			return user;

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}

	public User select(int id) {

		Connection connection = null;
		try {
			connection = DBUtil.getConnection();
			User user = new UserDao().select(connection, id);
			DBUtil.commit(connection);

			return user;
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}

	public List<UserBranchDepartment> select() {
		Connection connection = null;
		try {
			connection = DBUtil.getConnection();
			List<UserBranchDepartment> userBranchDepartments = new UserBranchDepartmentDao().select(connection);
			DBUtil.commit(connection);

			return userBranchDepartments;

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}

	public void update(User user) {

		Connection connection = null;
		try {

			if (!user.getPassword().isEmpty()) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);

			}
			connection = DBUtil.getConnection();
			new UserDao().update(connection, user);
			DBUtil.commit(connection);

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}

	public void switchStop(int id, int isStopped) {
		Connection connection = null;
		try {
			connection = DBUtil.getConnection();

			new UserDao().switchStop(connection, id, isStopped);
			DBUtil.commit(connection);

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}
}
