package service;

import java.sql.Connection;
import java.util.List;

import bean.Comment;
import bean.UserComment;
import dao.CommentDao;
import dao.UserCommentDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class CommentService {

	public void insert(Comment comment) {

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();
			new CommentDao().insert(connection, comment);
			DBUtil.commit(connection);

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}

	public List<UserComment> select() {

		Connection connection = null;
		try {
			connection = DBUtil.getConnection();
			List<UserComment> userComments = new UserCommentDao().select(connection);
			return userComments;

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);

			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}

	}

	public void delete(int deleteId) {
		Connection connection = null;
		try {
			connection = DBUtil.getConnection();
			new CommentDao().delete(connection, deleteId);
			DBUtil.commit(connection);

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);

			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}
}
