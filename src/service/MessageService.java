package service;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bean.Message;
import bean.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class MessageService {

	public List<UserMessage> select(String startDate, String endDate, String category) {

		if (!StringUtils.isBlank(startDate)) {
			startDate += " 00:00:00";

		} else {
			startDate = "2021-01-01 00:00:00";

		}

		if (!StringUtils.isBlank(endDate)) {
			endDate += " 23:59:59";

		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			endDate = String.valueOf(sdf.format(new Date()));

		}

		Connection connection = null;
		try {
			connection = DBUtil.getConnection();
			return new UserMessageDao().select(connection, startDate, endDate, category);

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);

			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);

			throw e;
		} finally {
			CloseableUtil.close(connection);

		}

	}

	public void insert(Message message) {
		Connection connection = null;
		try {
			connection = DBUtil.getConnection();
			new MessageDao().insert(connection, message);
			DBUtil.commit(connection);

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}

	public void delete(int deleteId) {
		Connection connection = null;
		try {
			connection = DBUtil.getConnection();
			new MessageDao().delete(connection, deleteId);
			DBUtil.commit(connection);

		} catch (Exception e) {
			DBUtil.rollback(connection);

		} catch (Error e) {
			DBUtil.rollback(connection);

		} finally {
			CloseableUtil.close(connection);
		}
	}

}
