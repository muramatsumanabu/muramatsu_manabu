package service;

import java.sql.Connection;
import java.util.List;

import bean.Department;
import dao.DepartmentDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class DepartmentService {

	public List<Department> select() {

		Connection connection = null;

		try {
			connection = DBUtil.getConnection();
			return  new DepartmentDao().select(connection);

		} catch (Exception e) {
			DBUtil.rollback(connection);
			throw e;

		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;

		} finally {
			CloseableUtil.close(connection);

		}
	}
}
