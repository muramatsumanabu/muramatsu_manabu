package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import bean.Branch;
import bean.Department;
import bean.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet("/signup")
public class SignUpServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("signup.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		User user = toUsers(request);
		String chkPassword = request.getParameter("chkPassword");
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(user, chkPassword, errorMessages)) {
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);

		} else {
			new UserService().insert(user);
			response.sendRedirect("management");

		}

	}

	private User toUsers(HttpServletRequest request) {
		User user = new User();

		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranch(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartment(Integer.parseInt(request.getParameter("departmentId")));

		return user;
	}

	public boolean isValid(User user, String chkPassword, List<String> errorMessages) {

		String account = user.getAccount();
		String password = user.getPassword();
		String name = user.getName();
		int branchId = user.getBranch();
		int departmentId = user.getDepartment();

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");

		} else if (!account.matches("^[a-zA-Z0-9_\\-.]{6,20}$")) {
			errorMessages.add("アカウント名は半角英数字は6字以上20文字以下で入力してください");

		} else {
			if (new UserService().select(account) != null) {
				errorMessages.add("既に使用されているアカウント名です");

			}

		}
		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力して下さい");

		} else if (!password.matches("^[a-zA-Z0-9.?/-]{6,20}$")) {
			errorMessages.add("パスワードは記号を含む半角文字で6文字以上20文字以下で入力してください");

		} else if (!chkPassword.equals(password)) {
			errorMessages.add("確認用パスワードに入力された値がパスワードと異なります");

		}

		if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");

		} else if (!name.matches("^.{1,10}$")) {
			errorMessages.add("名前は10文字以下で入力してください");

		}

		if (!((branchId == 1 && departmentId <= 2) || (branchId >= 2  && departmentId >= 3))) {
			errorMessages.add("支店名及び部署名の選択が不正です");

		}

		if (errorMessages.size() == 0) {
			return true;

		}

		return false;
	}

}