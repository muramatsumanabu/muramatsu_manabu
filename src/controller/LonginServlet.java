package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bean.User;
import service.UserService;

/**
 * Servlet implementation class LonginServlet
 */
@WebServlet("/login")
public class LonginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		List<String> errorMessages = new ArrayList<>();
		User user = null;

		if (isValid(account, password, errorMessages)) {
			 user = new UserService().select(account, password);

			if (user == null || user.getIsStopped() == 1) {
				errorMessages.add("ログインに失敗しました");

			}
		}
		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("account", account);
			request.getRequestDispatcher("login.jsp").forward(request, response);

		} else {
			HttpSession session = request.getSession();
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");

		}


	}

	public boolean isValid(String account, String password, List<String> errorMessages) {

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		}

		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");

		}
		if (errorMessages.size() == 0) {
			return true;

		}

		return false;
	}

}
