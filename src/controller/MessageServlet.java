package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bean.Message;
import bean.User;
import service.MessageService;

@WebServlet("/message")
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Message message = toMessage(request);
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(message, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("title", message.getTitle());
			request.setAttribute("category", message.getCategory());
			request.setAttribute("text", message.getText());
			request.getRequestDispatcher("message.jsp").forward(request, response);

		} else {
			new MessageService().insert(message);
			response.sendRedirect("./");

		}

	}

	public Message toMessage(HttpServletRequest request) {

		HttpSession session = request.getSession();
		int userId = ((User) session.getAttribute("loginUser")).getId();
		Message message = new Message();
		message.setTitle(request.getParameter("title"));
		message.setCategory(request.getParameter("category"));
		message.setText(request.getParameter("text"));
		message.setUserId(userId);

		return message;
	}

	public boolean isValid(Message message, List<String> errorMessages) {

		String title = message.getTitle();
		String category = message.getCategory();
		String text = message.getText();

		if (StringUtils.isBlank(title)) {
			errorMessages.add("件名を入力してください");

		} else if (title.length() > 30) {
			errorMessages.add("件名は30字以内で入力してください");

		}

		if (StringUtils.isBlank(category)) {
			errorMessages.add("カテゴリを入力してください");

		} else if (category.length() > 10) {
			errorMessages.add("カテゴリは10字以内で入力してください");

		}

		if (StringUtils.isBlank(text)) {
			errorMessages.add("本文を入力してください");

		} else if (text.length() > 1000) {
			errorMessages.add("本文は1000字以内で入力してください");

		}

		if (errorMessages.size() == 0) {
			return true;
		}

		return false;
	}

}
