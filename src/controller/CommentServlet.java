package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bean.Comment;
import bean.User;
import service.CommentService;

/**
 * Servlet implementation class CommentServlet
 */
@WebServlet("/comment")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Comment comment = toComment(request);
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(comment, errorMessages)) {
			HttpSession session = request.getSession();
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");

		} else {
			new CommentService().insert(comment);
			response.sendRedirect("./");
			
		}
	}

	public Comment toComment(HttpServletRequest request) {

		HttpSession session = request.getSession();
		Comment comment = new Comment();

		int userId = ((User) session.getAttribute("loginUser")).getId();
		comment.setText(request.getParameter("comment"));
		comment.setUserId(userId);
		comment.setMessageId(Integer.valueOf(request.getParameter("messageId")));

		return comment;
	}

	public boolean isValid(Comment comment, List<String> errorMessages) {

		String text = comment.getText();

		if (StringUtils.isBlank(text)) {
			errorMessages.add("コメントを入力してください");
			

		} else if (text.length() > 500) {
			errorMessages.add("コメントの投稿は500字以内で入力してください");
			
		}

		if (errorMessages.size() == 0) {
			return true;
			
		}

		return false;
	}

}
