package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.User;

@WebFilter(urlPatterns={"/management", "/setting", "/signup"})
public class ManagementFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		int userBranchId = ((User) session.getAttribute("loginUser")).getBranch();
		int userDepartmentId = ((User) session.getAttribute("loginUser")).getDepartment();

		List<String> errorMessages = new ArrayList<>();

		if ( userBranchId == 1 && userDepartmentId == 1) {
			chain.doFilter(request, response);

		} else {
			errorMessages.add("権限がありません");
			session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse) response).sendRedirect("./");

		}

	}

	public void init(FilterConfig fConfig){

	}

	public void destroy() {

	}
}
