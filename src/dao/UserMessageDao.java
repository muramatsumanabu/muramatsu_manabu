package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bean.UserMessage;
import exception.SQLRuntimeException;
import utils.CloseableUtil;

public class UserMessageDao {

	public List<UserMessage> select(Connection connection, String startDate, String endDate, String category) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    messages.id as id, ");
			sql.append("    users.account as account, ");
			sql.append("    messages.title as title, ");
			sql.append("    messages.category as category, ");
			sql.append("    messages.text as text, ");
			sql.append("    messages.user_id as user_id, ");
			sql.append("    messages.created_date as created_date, ");
			sql.append("    messages.updated_date as updated_date ");
			sql.append("FROM messages ");
			sql.append("JOIN users ");
			sql.append("	ON messages.user_id = users.id ");
			sql.append("WHERE messages.created_date");
			sql.append("	BETWEEN ? AND ? ");
			if (!StringUtils.isBlank(category)) {
				sql.append("AND messages.category ");
				sql.append("LIKE ? ");

			}
			sql.append("	ORDER BY messages.created_date DESC; ");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, startDate);
			ps.setString(2, endDate);

			if (!StringUtils.isBlank(category)) {
				String likeCategory = "%" + category + "%";
				ps.setString(3, likeCategory);

			}

			ResultSet rs = ps.executeQuery();

			List<UserMessage> messages = toUserMessages(rs);

			return messages;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			CloseableUtil.close(ps);

		}
	}

	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

		List<UserMessage> messages = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				UserMessage message = new UserMessage();

				message.setId(rs.getInt("id"));
				message.setAccount(rs.getString("account"));
				message.setTitle(rs.getString("title"));
				message.setCategory(rs.getString("category"));
				message.setText(rs.getString("text"));
				message.setUserId(rs.getInt("user_id"));
				message.setCreatedDate(rs.getTimestamp("created_date"));
				message.setUpdatedDate(rs.getTimestamp("updated_date"));

				messages.add(message);
			}
			return messages;

		} finally {
			CloseableUtil.close(rs);

		}
	}

}
