package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.Department;
import exception.SQLRuntimeException;
import utils.CloseableUtil;

public class DepartmentDao {
	public List<Department> select(Connection connection) {
		PreparedStatement ps = null;

		String sql = "SELECT * FROM departments;";
		try {
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Department> departments = toDepartmentes(rs);
			return departments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	private List<Department> toDepartmentes(ResultSet rs) throws SQLException {

		List<Department> departments = new ArrayList<>();
		try {
			while (rs.next()) {
				Department department = new Department();
				department.setId(rs.getInt("id"));
				department.setName(rs.getString("name"));
				department.setCreatedDate(rs.getDate("created_date"));
				department.setUpdatedDate(rs.getDate("updated_date"));
				departments.add(department);
			}
			return departments;
		} finally {
			CloseableUtil.close(rs);
		}
	}
}
