package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> select(Connection connection) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("	comments.id, ");
			sql.append("	users.id, ");
			sql.append("	users.account, ");
			sql.append("	comments.text, ");
			sql.append("	comments.user_id, ");
			sql.append("	comments.message_id, ");
			sql.append("	comments.created_date, ");
			sql.append("	comments.updated_date ");
			sql.append("FROM comments ");
			sql.append("JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date ASC");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			List<UserComment> userComments = toUserMessages(rs);

			return userComments;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			try {
				ps.close();

			} catch (SQLException e) {
				e.printStackTrace();

			}
		}
	}

	private List<UserComment> toUserMessages(ResultSet rs) throws SQLException {

		List<UserComment> userComments = new ArrayList<>();
		try {
			while (rs.next()) {
				UserComment userComment = new UserComment();

				userComment.setId(rs.getInt("comments.id"));
				userComment.setAccount(rs.getString("users.account"));
				userComment.setText(rs.getString("comments.text"));
				userComment.setUserId(Integer.parseInt(rs.getString("comments.user_id")));
				userComment.setMessageId(Integer.parseInt(rs.getString("comments.message_id")));
				userComment.setCreatedDate(rs.getTimestamp("comments.created_date"));
				userComment.setUpdatedDate(rs.getTimestamp("comments.updated_date"));

				userComments.add(userComment);
			}
			return userComments;

		} finally {
			rs.close();

		}
	}

}
