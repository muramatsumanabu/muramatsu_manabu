package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.UserBranchDepartment;
import exception.SQLRuntimeException;
import utils.CloseableUtil;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT");
			sql.append("	users.id, ");
			sql.append("	users.account, ");
			sql.append("	users.name, ");
			sql.append("	branches.name ,");
			sql.append("	departments.name, ");
			sql.append("	users.is_stopped, ");
			sql.append("    users.created_date, ");
			sql.append("    users.updated_date ");
			sql.append("FROM users ");
			sql.append("JOIN branches ON users.branch_id = branches.id ");
			sql.append("JOIN departments ON users.department_id = departments.id ");
			sql.append("ORDER BY users.created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> userBranchDepartments = toUserBranchDepartment(rs);

			return userBranchDepartments;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
			
		} finally {
			CloseableUtil.close(ps);
			
		}

	}

	private List<UserBranchDepartment> toUserBranchDepartment(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> userBranchDepartments = new ArrayList<>();
		try {
			while (rs.next()) {
				UserBranchDepartment userBranchDeaprtment = new UserBranchDepartment();
				userBranchDeaprtment.setId(rs.getInt("users.id"));
				userBranchDeaprtment.setAccount(rs.getString("users.account"));
				userBranchDeaprtment.setName(rs.getString("name"));
				userBranchDeaprtment.setBranch(rs.getString("branches.name"));
				userBranchDeaprtment.setIsStopped(rs.getInt("is_stopped"));
				userBranchDeaprtment.setDepartment(rs.getString("departments.name"));
				userBranchDeaprtment.setCreatedDate(rs.getTimestamp("created_date"));
				userBranchDeaprtment.setUpdatedate(rs.getTimestamp("updated_date"));
				userBranchDepartments.add(userBranchDeaprtment);
			}
			return userBranchDepartments;

		} finally {
			CloseableUtil.close(rs);
			
		}
	}

}
