package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.Branch;
import exception.SQLRuntimeException;
import utils.CloseableUtil;

public class BranchDao {

	public List<Branch> select(Connection connection) {
		PreparedStatement ps = null;

		String sql = "SELECT * FROM branches;";
		try {
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Branch> branches = toBranches(rs);
			return branches;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	private List<Branch> toBranches(ResultSet rs) throws SQLException {

		List<Branch> branchs = new ArrayList<>();
		try {
			while (rs.next()) {
				Branch branch = new Branch();
				branch.setId(rs.getInt("id"));
				branch.setName(rs.getString("name"));
				branch.setCreatedDate(rs.getDate("created_date"));
				branch.setUpdatedDate(rs.getDate("updated_date"));
				branchs.add(branch);
			}
			return branchs;
		} finally {
			CloseableUtil.close(rs);
		}
	}
}