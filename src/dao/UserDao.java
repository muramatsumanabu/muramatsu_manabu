package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.User;
import exception.SQLRuntimeException;
import utils.CloseableUtil;

public class UserDao {

	public void insert(Connection connection, bean.User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("    account, ");
			sql.append("    password, ");
			sql.append("    name, ");
			sql.append("    branch_id, ");
			sql.append("    department_id , ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, "); // account
			sql.append("    ?, "); // password
			sql.append("    ?, "); // name
			sql.append("    ?, "); // branch
			sql.append("    ?, "); // department
			sql.append("    CURRENT_TIMESTAMP, "); // created_date
			sql.append("    CURRENT_TIMESTAMP "); // updated_date
			sql.append(");");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getDepartment());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			CloseableUtil.close(ps);

		}
	}

	public User select(Connection connection, String account, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, account);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if (users.isEmpty()) {
				return null;
			} else if (2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");

			} else {
				return users.get(0);

			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			CloseableUtil.close(ps);

		}
	}

	public User select(Connection connection, String account) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if (users.isEmpty()) {
				return null;
			} else if (2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");
			} else {
				return users.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			CloseableUtil.close(ps);

		}
	}

	public User select(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if (users.isEmpty()) {
				return null;

			} else if (2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");

			} else {
				return users.get(0);

			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			CloseableUtil.close(ps);

		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;

		try {

			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE ");
			sql.append("	users ");
			sql.append("SET ");
			sql.append("    account = ?, ");
			if (!user.getPassword().isEmpty()) {
				sql.append("    password = ?, ");
			}
			sql.append("    name = ?, ");
			sql.append("    branch_id = ?, ");
			sql.append("    department_id = ?, ");
			sql.append("    updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE id = ? ;");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());

			if (!user.getPassword().isEmpty()) {
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setInt(4, user.getBranch());
				ps.setInt(5, user.getDepartment());
				ps.setInt(6, user.getId());

			} else {
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranch());
				ps.setInt(4, user.getDepartment());
				ps.setInt(5, user.getId());

			}

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			CloseableUtil.close(ps);

		}
	}

	public void switchStop(Connection connection, int id, int isStopped) {

		PreparedStatement ps = null;
		try {
			String sql = "UPDATE users SET is_stopped = ? WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, isStopped);
			ps.setInt(2, id);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			CloseableUtil.close(ps);

		}
	}



	private List<User> toUsers(ResultSet rs) throws SQLException {

		List<User> users = new ArrayList<User>();
		try {
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranch(Integer.valueOf(rs.getString("branch_id")));
				user.setDepartment(Integer.valueOf(rs.getString("department_id")));
				user.setIsStopped(Integer.valueOf(rs.getString("is_stopped")));
				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setUpdatedDate(rs.getTimestamp("updated_date"));

				users.add(user);
			}
			return users;

		} finally {
			CloseableUtil.close(rs);

		}
	}

}