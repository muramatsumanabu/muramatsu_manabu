<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
.header {
	margin: 25px 30% 25px 30%;
}

.post {
	margin: 25px 30% 25px 30%;
	padding: 15px;
	background-color: #c0c0c0;
	border-radius: 20px;
}

p {
	text-align: center;
}
</style>
<title>新規投稿画面</title>
</head>
<body>

	<a href="./">ホーム</a>
	<c:if test="${not empty errorMessages}">
		<ul>
			<c:forEach items="${errorMessages}" var="errorMessage">
			 <li><c:out value="${errorMessage}"></c:out>
			</c:forEach>
		</ul>
	</c:if>
	<form action ="message" method ="post">
		<label for= "title">件名</label><input type="text" name="title" id="title" value="${title}"><br>
		<label for= "category">カテゴリ</label><input type="text" name="category" id="category"  value="${category}"><br>
		<label for= "text">本文<br></label><textarea cols="20" rows="10" name="text" id="text" ><c:out value="${text}" /></textarea><br>
		<input type="submit" value="投稿">
	</form>
	<p>Copyright(c)MuramatsuManabu</p>

</body>
</html>