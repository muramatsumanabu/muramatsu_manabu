<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集画面</title>
<style>
.header {
	margin: 25px 30% 25px 30%;
}

.post {
	margin: 25px 30% 25px 30%;
	padding: 15px;
	background-color: #c0c0c0;
	border-radius: 20px;
}

p {
	text-align: center;
}
</style>
</head>
<body>
	<div class="header">
		<a href="management">ユーザー管理</a> <br>

		<c:if test="${not empty errorMessages}">
			<c:forEach items="${errorMessages}" var="errorMessage">
				<ul>
					<li><c:out value="${errorMessage}" />
				</ul>
			</c:forEach>
		</c:if>
	</div>
	<div class="post">
	<form action="setting" method="post">
		<input type="hidden" name="id" value="${user.id}" id="id" /> <label
			for="account">アカウント名</label> <input type="text" name="account"
			value="${user.account}" id="account" />アカウント名は半角英数字は6字以上20文字以下で入力してください<br />
		<label for="password">パスワード</label> <input name="password"
			type="password" id="password" />パスワードは記号を含む半角文字で6文字以上20文字以下で入力してください
		<br /> <label for="chkPassword">確認パスワード</label> <input
			name="chkPassword" type="password" id="chkPassword" /><br> <label
			for="name">名前</label> <input name="name" value="${user.name}"
			id="name" />名前は10文字以下で入力してください<br> 支社 <select name="branchId">
			<c:forEach items="${branches}" var="branch">
				<c:choose>
					<c:when test="${loginUser.id != user.id}">
						<option value="${branch.id}"
							<c:if test="${user.branch == branch.id}">selected</c:if>><c:out
								value="${branch.name}" />
					</c:when>
					<c:otherwise>
						<option value="${branch.id}"
							<c:if test="${user.branch != branch.id}">disabled</c:if>><c:out
								value="${branch.name}" />
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</select><br> 部署 <select name="departmentId">
			<c:forEach items="${departments}" var="department">
				<c:choose>
					<c:when test="${loginUser.id != user.id}">
						<option value="${department.id}"
							<c:if test="${user.department == department.id}">selected</c:if>><c:out
								value="${department.name}" />
					</c:when>
					<c:otherwise>
						<option value="${department.id}"
							<c:if test="${user.department != department.id}">disabled</c:if>><c:out
								value="${department.name}" />
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</select><br> <input type="submit" value="更新">
	</form>
	</div>
	<p>Copyright(c)MuramatsuManabu</p>
</body>
</html>