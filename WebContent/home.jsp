<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
<style type="text/css">
.header {
	margin: 25px 30% 25px 30%;
}

.post {
	margin: 25px 30% 25px 30%;
	padding: 15px;
	background-color: #c0c0c0;
	border-radius: 20px;
}

p {
	text-align: center;
}
</style>
</head>
<body>
	<div class="header">
		<a href="message">新規投稿</a>
		<c:if test="${loginUser.branch == 1 && loginUser.department == 1}">
			<a href="management">ユーザー管理</a>
		</c:if>
		<a href="login">ログイン</a> <a href="logout">ログアウト</a> <br>

		<form action="./" method="get">
			日付: <input type="date" name="startDate" value="${startDate}" />～ <input
				type="date" name="endDate" value="${endDate}" /><br> カテゴリ: <input
				type="text" name="category" value="${category}" /> <input
				type="submit" value="絞り込み" />
		</form>
	</div>

	<c:if test="${not empty errorMessages}">
		<ul>
			<c:forEach items="${errorMessages}" var="errorMessage">
				<li><c:out value="${errorMessage}"></c:out> <c:remove
						var="errorMessages" scope="session" />
			</c:forEach>
		</ul>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

	<c:forEach items="${messages}" var="message">
		<div class="post">
			ユーザー名:
			<c:out value="${message.account}" />
			<br> 件名:
			<c:out value="${message.title}" />
			<br> カテゴリ:
			<c:out value="${message.category}" />
			<br>投稿内容:<br>
			<c:forEach var="text" items="${fn:split(message.text, '
			')}">
				<div>
					<c:out value="${text}" />
				</div>
			</c:forEach>
			<br> 投稿日時:
			<fmt:formatDate value="${message.createdDate}"
				pattern="yyyy/MM/dd HH:mm:ss" />
			<br> 更新日時:
			<fmt:formatDate value="${message.updatedDate}"
				pattern="yyyy/MM/dd HH:mm:ss" />
			<br>
			<c:if test="${message.userId == loginUser.id}">
				<form action="deleteMessage" method="post"
					onSubmit="return checkSubmit()">
					<input type="hidden" name="deleteMessageId" value="${message.id}">
					<input type="submit" value="削除">
				</form>
			</c:if>
			<c:forEach items="${comments}" var="comment">
				<c:if test="${message.id == comment.messageId}">
				ユーザー名:<c:out value="${comment.account}" />
					<br>
				コメント内容:<br>
					<c:forEach var="commentText"
						items="${fn:split(comment.text, '
				')}">
						<div>
							<c:out value="${commentText}" />
						</div>
					</c:forEach>
				投稿日時:<c:out value="${comment.createdDate}" />
					<br>
				更新日時:<c:out value="${comment.updatedDate}" />
					<br>
					<c:if test="${comment.userId == loginUser.id}">
						<form action="deleteComment" method="post"
							onSubmit="return checkSubmit()">
							<input type="hidden" name="deleteCommentId" value="${comment.id}">
							<input type="submit" value="削除">
						</form>
					</c:if>
				</c:if>
			</c:forEach>

			<form action="comment" method="post">
				コメント<br> <input type="hidden" name="userId"
					value="${message.userId}" /> <input type="hidden" name="messageId"
					value="${message.id}" />
				<textarea cols="45" rows="10" name="comment"></textarea>
				<br> <input type="submit" value="コメント投稿"><br>
			</form>
		</div>
	</c:forEach>
	<p>Copyright(c)MuramatsuManabu</p>

	<script type="text/javascript">
		function checkSubmit() {
			return confirm("本当に削除しますか？");
		}
	</script>


</body>
</html>