<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
.header {
	margin: 25px 30% 25px 30%;
}

.post {
	margin: 25px 15% 25px 15%;
	padding: 15px;
	background-color: #c0c0c0;
	border-radius: 20px;
}

p {
	text-align: center;
}
</style>
<title>ユーザ新規登録</title>
</head>
<body>
	<div class="header">
		<a href="management">ユーザ管理</a>
	</div>
	<c:if test="${not empty errorMessages}">
		<ul>
			<c:forEach items="${errorMessages}" var="errorMessage">
				<li><c:out value="${errorMessage}"></c:out>
			</c:forEach>
		</ul>
	</c:if>
	<div class="post">
		<form action="signup" method="post">
			<label for="account">アカウント</label><input type="text" name="account"
				id="account" value="${user.account}" />アカウント名は半角英数字は6字以上20文字以下で入力してください<br>
			<label for="password">パスワード</label><input name="password"
				type="password" id="password" />パスワードは記号を含む半角文字で6文字以上20文字以下で入力してください<br>
			<label for="chkPassword">確認パスワード</label><input name="chkPassword"
				type="password" id="chkPassword" /><br> <label for="name">名前</label><input
				type="text" name="name" id="name" value="${user.name}" />名前は10文字以下で入力してください<br>
			支社 <select name="branchId">
				<c:forEach items="${branches}" var="branch">
					<option value="${branch.id}"
						<c:if test="${user.branch == branch.id}">selected</c:if>><c:out
							value="${branch.name}" /></option>
				</c:forEach>
			</select><br> 部署 <select name="departmentId">
				<c:forEach items="${departments}" var="department">
					<option value="${department.id}"
						<c:if test="${user.department == department.id}">selected</c:if>><c:out
							value="${department.name}" /></option>
				</c:forEach>
			</select><br> <input type="submit" value="登録" />
		</form>
	</div>
	<p>Copyright(c)MuramatsuManabu</p>


</body>
</html>