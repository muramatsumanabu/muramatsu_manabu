<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="styleSheet.css">
<title>ユーザー管理画面</title>
<style type="text/css">
.header {
	margin: 25px 30% 25px 30%;
}

.post {
	margin: 25px 30% 25px 30%;
	padding: 15px;
	background-color: #c0c0c0;
	border-radius: 20px;
}

p {
	text-align: center;
}
</style>
</head>
<body>
	<div class="header">
		<a href="./">ホーム</a> <a href="signup">ユーザ新規登録</a> <br>
		<c:if test="${not empty errorMessages}">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}"></c:out>
				</c:forEach>
			</ul>
			<c:remove var="errorMessages" />
		</c:if>
	</div>
	<div class="post">
		<c:forEach items="${userBranchDepartments}" var="userBranchDepartment">
			<br>
		アカウント名：<c:out value="${userBranchDepartment.account}" />
			<br>
		名前：<c:out value="${userBranchDepartment.name}" />
			<br>
		支社：<c:out value="${userBranchDepartment.branch}" />
			<br>
		部署：<c:out value="${userBranchDepartment.department}" />
			<br>
		アカウント停止状態：
		<c:choose>
				<c:when test="${userBranchDepartment.isStopped == 0}">
					<div>停止されていません</div>
				</c:when>
				<c:when test="${userBranchDepartment.isStopped == 1}">
					<div>停止されています</div>
				</c:when>
			</c:choose>
			<form action="setting" method="get">
				<input type="hidden" value="${userBranchDepartment.id}"
					name="editUserId"> <input type="submit" value="編集"><br>
			</form>
			<c:if test="${loginUser.id != userBranchDepartment.id}">
				<c:if test="${userBranchDepartment.isStopped == 0}">
					<form action="stop" method="post" onSubmit="return stopUser()">
						<input type="hidden" value="${userBranchDepartment.id}"
							name="editUserId"> <input type="hidden" value="1"
							name="isStopped"> <input type="submit" value="停止"><br>
					</form>
				</c:if>
				<c:if test="${userBranchDepartment.isStopped == 1}">
					<form action="stop" method="post" onSubmit="return restartUser()">
						<input type="hidden" value="${userBranchDepartment.id}"
							name="editUserId"> <input type="hidden" value="0"
							name="isStopped"> <input type="submit" value="復活"><br>
					</form>
				</c:if>
			</c:if>
		</c:forEach>
	</div>
	<p>Copyright(c)MuramatsuManabu</p>
	<script type="text/javascript">
		function stopUser() {
			return confirm("本当に停止しますか？");
		}
	</script>
	<script type="text/javascript">
		function restartUser() {
			return confirm("本当に復活しますか？");
		}
	</script>


</body>
</html>