<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
.header {
	margin: 25px 30% 25px 30%;
}

.post {
	margin: 25px 30% 25px 30%;
	padding: 15px;
	background-color: #c0c0c0;
	border-radius: 20px;
}

p {
	text-align: center;
}
</style>
<title>ログイン画面</title>
</head>
<body>
	<div class="header">
		<c:if test="${not empty errorMessages}">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}"></c:out> <c:remove
							var="errorMessages" scope="session" />
				</c:forEach>
			</ul>
		</c:if>
	</div>
	<div class="post">
		<form action="login" method="post">
			<label for="account">アカウント</label><input type="text" name="account"
				id="account" value="${account}" /><br> <label for="password">パスワード</label><input
				name="password" type="password" id="password" /><br> <input
				type="submit" value="ログイン" />
		</form>
	</div>
	<p>Copyright(c)MuramatsuManabu</p>
</body>
</html>